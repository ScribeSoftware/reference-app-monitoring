/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
*/


/* Contents: functions for Connecting to the Scribe Platform 
*/

function getConnected() {
    //Clear the ORGS and Solutions
    $('#divSelectOrg').hide(); 
    $('#selectOrg').empty(); 
    $('#divSelectSolution').hide(); 
    $('#selectSolution').empty(); 
    $('#divShowSolutionHistory').hide(); 

    //Clears information about the logged in USER
    var divSuccess = $('#divUserDetail').find('.alert-success');
    var divFailure = $('#divUserDetail').find('.alert-danger');
    divSuccess.empty();
    divSuccess.slideUp();
    divFailure.empty();
    divFailure.slideUp();

    //Reset the Monitoring control section of the Screen 
    resetMonitoringUI(); 

     //Get the Credentials from the FORM
     APIUSERNAME = $('#usr').val();
     APIPASSWORD = $('#pwd').val();

    $.ajax({
        type: "GET",
        url: APIURL,
        headers: {
            "Authorization": "Basic " + btoa(APIUSERNAME + ":" + APIPASSWORD)
        },
        data: { },
        dateType: 'json',
        success: function (result) {
            
          
            var divSuccess = $('#divLoginUser').find('.alert-success');
             divSuccess.append("Session Connected for " + APIUSERNAME);
             divSuccess.slideDown(); 
             
             //Build a list box of ORGS for the authenticated user
             getOrgsForSelection();

             //Set the first Org to the Active Org 
                $('#selectOrg').prop('selectedIndex', 1); 
                $('#selectOrg').trigger('change'); 
            
            var orgId = $('#divSelectOrg').find('#selectOrg').val();
                $('#divSelectSolution').show(); 
                $('#divShowSolutionHistory').show(); 
           

             
        },

        error: function (xhr, error) {
            console.log(xhr);
            console.log(error);
            var divFailure = $('#divLoginUser').find('.alert-danger');
            divFailure.append("Unable to connect " + APIUSERNAME );
            $('#pwd').val("");
            divFailure.slideDown();
        }
    });
}

